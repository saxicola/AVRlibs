/*
 * system_clock.h
 *
 * Copyright (c) 2014 Igor Mikolic-Torreira.  All right reserved.
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/atomic.h>


//Timer malarky from AVRTools/SystemClock.*
#define clockCyclesPerMicrosecond()         ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds( a )      ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles( a )      ( (a) * clockCyclesPerMicrosecond() )
volatile unsigned long      timer0_overflow_count = 0;
volatile unsigned long      timer0_millis = 0;
uint8_t                     timer0_fract = 0;
const unsigned long kMicrosecondsPerOverflowTIMER0 = ( clockCyclesToMicroseconds( 64 * 256 ) );
// The whole number of milliseconds per timer0 overflow
const unsigned long kMillisInc = ( clockCyclesToMicroseconds( 64 * 256 ) / 1000 );
// The fractional number of milliseconds per timer0 overflow. Shift right
// by three to fit these numbers into a byte (for 8 MHz and 16 MHz this doesn't lose precision).
const uint8_t kFractInc =  ( ( (clockCyclesToMicroseconds( 64 * 256)) % 1000 ) >> 3 );
const uint8_t kFractMax =  ( 1000 >> 3 );

void initSystemClock(void);
uint32_t millis(void);
uint32_t micros(void);
