var searchData=
[
  ['adc_2ec',['adc.c',['../adc_8c.html',1,'']]],
  ['adc_2eh',['adc.h',['../adc_8h.html',1,'']]],
  ['adc_5f328_5fread',['ADC_328_read',['../adc_8c.html#aa3108cf878335e4c2477802e157415ed',1,'ADC_328_read(uint8_t ADCchannel):&#160;adc.c'],['../adc_8h.html#aa3108cf878335e4c2477802e157415ed',1,'ADC_328_read(uint8_t ADCchannel):&#160;adc.c']]],
  ['adc_5f8_5fread',['ADC_8_read',['../adc_8c.html#a6a4eee03d1e183ab4d886f088d22489a',1,'ADC_8_read(uint8_t channel):&#160;adc.c'],['../adc_8h.html#a3681b8ef327071942560d926beff705b',1,'ADC_8_read(uint8_t ADCchannel):&#160;adc.c']]],
  ['adc_5fdisable',['ADC_DISABLE',['../adc_8h.html#adc5911f4f1e4a81fb3381359bf74e4c2',1,'adc.h']]],
  ['adc_5fenable',['ADC_ENABLE',['../adc_8h.html#af0f7ac4025d1623cda58f7656fcffade',1,'adc.h']]],
  ['adc_5fstart_5fconversion',['ADC_START_CONVERSION',['../adc_8h.html#a0b1602e1735dd932d052125db2b6fc12',1,'adc.h']]]
];
