var searchData=
[
  ['pin',['pin',['../df__player_8c.html#ab40a673fb19c1e650e1f79de91788aa5',1,'df_player.c']]],
  ['play_5fas_5fbackground',['play_as_background',['../bluestone_8c.html#ae20707d3321f11bf451b1e676d8949d8',1,'play_as_background(uint8_t track, uint8_t volume):&#160;bluestone.c'],['../bluestone_8h.html#ae20707d3321f11bf451b1e676d8949d8',1,'play_as_background(uint8_t track, uint8_t volume):&#160;bluestone.c']]],
  ['play_5frandom',['play_random',['../bluestone_8c.html#ac587d1b1d87a994802a5e4b57fe996bc',1,'play_random(uint8_t volume):&#160;bluestone.c'],['../bluestone_8h.html#ac587d1b1d87a994802a5e4b57fe996bc',1,'play_random(uint8_t volume):&#160;bluestone.c']]],
  ['play_5ftrack',['play_track',['../bluestone_8c.html#abbeb1c8f434ee2fea9b19980e9d6ce6b',1,'play_track(uint8_t track_num, uint8_t volume):&#160;bluestone.c'],['../bluestone_8h.html#abbeb1c8f434ee2fea9b19980e9d6ce6b',1,'play_track(uint8_t track_num, uint8_t volume):&#160;bluestone.c']]]
];
