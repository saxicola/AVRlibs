var searchData=
[
  ['send_5ffunc',['send_func',['../df__player_8c.html#a7bcfc4d03fee5c6933d75a2aa9cf101b',1,'send_func():&#160;df_player.c'],['../df__player_8h.html#a99b5f21241a765a58f944de4af965097',1,'send_func(void):&#160;df_player.c']]],
  ['sendbyserial',['sendBySerial',['../df__player_8c.html#a996f36a32c6921d63d3fd6fc31f716a1',1,'sendBySerial(const uint8_t data):&#160;df_player.c'],['../df__player_8h.html#a996f36a32c6921d63d3fd6fc31f716a1',1,'sendBySerial(const uint8_t data):&#160;df_player.c']]],
  ['sendstrbyserial',['sendStrBySerial',['../df__player_8c.html#a6598ccd1b16e02413ac04dd15464d790',1,'sendStrBySerial(char *p):&#160;df_player.c'],['../df__player_8h.html#a6598ccd1b16e02413ac04dd15464d790',1,'sendStrBySerial(char *p):&#160;df_player.c']]],
  ['start_5frepeat',['start_repeat',['../df__player_8c.html#ab69d2428ac73773d8635886b2f1ef811',1,'start_repeat():&#160;df_player.c'],['../df__player_8h.html#aad21d38d6aafb23538fd0706b6bea8dd',1,'start_repeat(void):&#160;df_player.c']]],
  ['stop_5frepeat',['stop_repeat',['../df__player_8c.html#a45024ecc40ae9812ae4a4fe06b4e5431',1,'stop_repeat():&#160;df_player.c'],['../df__player_8h.html#a44acacd2ed5a82df70c416e59553eff8',1,'stop_repeat(void):&#160;df_player.c']]],
  ['stp16_5finit',['stp16_init',['../stp16cp05_8c.html#ad8cc3a80f4231d195197db532f708c93',1,'stp16_init(volatile uint8_t *port, volatile uint8_t *ddr, uint8_t sdi, uint8_t clk, uint8_t le, uint8_t oe):&#160;stp16cp05.c'],['../stp16cp05_8h.html#ad8cc3a80f4231d195197db532f708c93',1,'stp16_init(volatile uint8_t *port, volatile uint8_t *ddr, uint8_t sdi, uint8_t clk, uint8_t le, uint8_t oe):&#160;stp16cp05.c']]],
  ['stp16_5foff',['stp16_off',['../stp16cp05_8c.html#a479e834ed73b9d30973cf475cd9613dc',1,'stp16_off():&#160;stp16cp05.c'],['../stp16cp05_8h.html#ac6253a9b93623d02709872fabd0cb14b',1,'stp16_off(void):&#160;stp16cp05.c']]],
  ['stp16_5fon',['stp16_on',['../stp16cp05_8c.html#add1043ed0ed90c6564f22fd952d74acb',1,'stp16_on():&#160;stp16cp05.c'],['../stp16cp05_8h.html#a6bcfa988b80a4673dca6df102985639d',1,'stp16_on(void):&#160;stp16cp05.c']]],
  ['stp16_5fset_5fleds',['stp16_set_leds',['../stp16cp05_8c.html#ad85b3b9b77339ad14f081047e21d6597',1,'stp16_set_leds(uint16_t *leds, uint8_t len):&#160;stp16cp05.c'],['../stp16cp05_8h.html#ad85b3b9b77339ad14f081047e21d6597',1,'stp16_set_leds(uint16_t *leds, uint8_t len):&#160;stp16cp05.c']]]
];
