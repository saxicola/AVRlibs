/*
 * bluestone.h
 * 
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 * 
 * 
 */
 
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/sfr_defs.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "df_player.h"

#ifndef BLUESTONE_H
#define BLUESTONE_H

// Pin definitions.
#define OUT_A         PA3
#define BACKGROUND 1 // The background sound file sdcard/mp3/0001.mp3

uint16_t file_count;
uint8_t backgound_playing;

void initSystemClock(void);
unsigned long millis(void);
unsigned long micros(void);
void calibrate(void);
uint16_t gen_random(uint8_t max);
uint16_t get_file_count(void);
void play_random(uint8_t volume);
void play_as_background(uint8_t track, uint8_t volume);
void play_track(uint8_t track_num,uint8_t volume);
#endif
