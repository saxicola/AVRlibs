/*
 * bluestone.c
 * 
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 * 
 * 
 */

#include "bluestone.h"


//Timer malarky from AVRTools/SystemClock.*
#define clockCyclesPerMicrosecond()         ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds( a )      ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles( a )      ( (a) * clockCyclesPerMicrosecond() )
volatile unsigned long      timer0_overflow_count = 0;
volatile unsigned long      timer0_millis = 0;
uint8_t                     timer0_fract = 0;
const unsigned long kMicrosecondsPerOverflowTIMER0 = ( clockCyclesToMicroseconds( 64 * 256 ) );
// The whole number of milliseconds per timer0 overflow
const unsigned long kMillisInc = ( clockCyclesToMicroseconds( 64 * 256 ) / 1000 );
// The fractional number of milliseconds per timer0 overflow. Shift right
// by three to fit these numbers into a byte (for 8 MHz and 16 MHz this doesn't lose precision).
const uint8_t kFractInc =  ( ( (clockCyclesToMicroseconds( 64 * 256)) % 1000 ) >> 3 );
const uint8_t kFractMax =  ( 1000 >> 3 );


ISR( TIMER0_OVF_vect )
{
    // Copy these to local variables so they can be stored in registers
    // (volatile variables must be read from memory on every access)
    unsigned long m = timer0_millis;
    uint8_t f = timer0_fract;

    m += kMillisInc;
    f += kFractInc;
    if ( f >= kFractMax )
    {
        f -= kFractMax;
        ++m;
    }

    timer0_fract = f;
    timer0_millis = m;
    timer0_overflow_count++;
}

void initSystemClock()
{
    ATOMIC_BLOCK( ATOMIC_RESTORESTATE )
    {
        // Use Timer0 for the system clock, but configure it so it also supports
        // fast hardware pwm (using phase-correct PWM would mean that Timer0
        // overflowed half as often)

        TCCR0A = 0;     // Clear all settings
        TCCR0B = 0;     // Clear all settings
        TIMSK = 0;     // Disable all interrupts
        TCNT0  = 0;     // initialize counter value to 0

        TCCR0A |= (1 << WGM01) | (1 << WGM00);

        // set timer 0 prescale factor to 64
        TCCR0B |= (1 << CS01) | (1 << CS00);

        // enable timer 0 overflow interrupt
        TIMSK |= (1 << TOIE0);
    }
}

unsigned long millis()
{
    // Disable interrupts while we read timer0_millis or we might get an
    // inconsistent value (e.g. in the middle of a write to timer0_millis)
    unsigned long m;

    ATOMIC_BLOCK( ATOMIC_RESTORESTATE )
    {
        m = timer0_millis;
    }

    return m;
}


unsigned long micros()
{
    // Disable interrupts to avoid reading inconsistent values
    unsigned long m;
    uint8_t t;
    ATOMIC_BLOCK( ATOMIC_RESTORESTATE )
    {
        m = timer0_overflow_count;
        t = TCNT0;

        if ( ( TIFR & _BV(TOV0) ) && ( t < 255 ) )
        {
            m++;
        }
    }
    return ( (m << 8) + t ) * ( 64 / clockCyclesPerMicrosecond() );
}




/*
Generate a 'random' from 2 to max number
This never returns 1 as a 1 is never needed here.
*/
#define RAND_INV_RANGE(r) ((int) ((RAND_MAX + 1) / (r)))
uint16_t gen_random(uint8_t max)
{
    uint8_t rnd = 0;
    rnd = rand() % (max+1);
    initSerial(OUT_A);
    return rnd+2; 
}


/*
Get track count by trying to play tracks sequentially and testing if the busy
pin goes low for a time. If it does the track exists, if not then that's trackcount +1
This only needs to run once on bootup. Although a SDCard change may be done
while powered up this is unlikely to be an issue 'in the field'.
If there's no Rx serial pin this will work even though it's a little clunky.
WARNING: This doesn't skip tracks so make sure they start at 0001.mp3 and are serially
numbered.
@return uint8_t Track count.
*/
uint16_t get_file_count()
{
    uint16_t num = 1;
    uint8_t n;
    num = 1;
    
    for(n = 9; n-- ; n) // Arbitary limit
    {
        initSerial(OUT_A); _delay_ms(10);
        mp3_set_volume(0);
        mp3_play_in_mp3 (num); 
        DDRB &= ~(1 << OUT_A); // Set pin to input.
        initSystemClock();
        _delay_ms(500); // Just long enough to allow the player to start.
        // Test if it's high, if it is then the track hasn't played and doesn't exist.        
        if(PINB & (1 << OUT_A))
        {
            initSerial(OUT_A);
            mp3_stop();
            continue;
        } //Try again
        else
        {
            initSerial(OUT_A);
            mp3_stop();
            initSystemClock();
            _delay_ms(500);
            num++;
        }
    }
    
    initSystemClock();
    
    return num - 1;
}

void play_random(uint8_t volume)
{
    // Save the counter
    uint16_t count = TCNT0;
    uint8_t track;
    char buff[] = "01234567890123";
    backgound_playing = 0;
    initSerial(OUT_A); _delay_ms(10);
    mp3_set_volume(volume);
    stop_repeat();
    _delay_ms(50);
    if(file_count < 2) track = 1;   
    else track = gen_random(file_count);
    // DEBUGGING
    sprintf(buff, "\nrand = %d\n",track);
    sendStrBySerial(buff);
    sendBySerial(0xEF);    

    DDRB |= (1 << OUT_A); // Set pin to output.    
    mp3_play_in_mp3 (track);
    DDRB &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(100);
    
    // Revert to normal timer
    initSystemClock();
    // and restore counter
    TCNT0 = count;
    // PCINT0 can be/is connected to BUSY pin
    DDRB &= ~(1 << OUT_A); // Set it as input so we can monitor it's status.
}

void play_as_background(uint8_t track, uint8_t volume)
{
    uint16_t count = TCNT0;
    PCMSK &= ~(1 << PCINT0); // Disable PCINT0
    initSerial(OUT_A); _delay_ms(10);
    mp3_set_volume(volume);
    mp3_play_in_mp3 (track);
    start_repeat(); // Repeat currently playing track
    // Revert to normal timer
    initSystemClock();
    // and restore counter
    TCNT0 = count;
    DDRB &= ~(1 << OUT_A); // So set it as input
}

/**
Play a specified frack in the mp3 folder
@param uint8_t track_num
@param uint8_t volume
*/
void play_track(uint8_t track_num, uint8_t volume)
{
    // Save the counter
    uint16_t count = TCNT0;
    uint32_t timer;
    initSerial(OUT_A); _delay_ms(10);
    mp3_set_volume(volume);
    mp3_play_in_mp3 (track_num);
    DDRB &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(100);
    initSystemClock(); // So we can add a timeout.
    TCNT0 = count;
    timer = millis();    
}

