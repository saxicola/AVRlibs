/*
 * df_player.c
 *
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Functions for controlling a df_player mini.
  This has to have 1MHz clock speed and clock div = 8
  TODO: Refuse to compile if not the case.
  TODO: May have to disable watchdog in here.  This will require saving the WD settings.
 */



#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "df_player.h"
#include "system_clock.h"
#include <avr/wdt.h>


static void fill_uint16_bigend (uint8_t *thebuf, uint16_t data);
uint16_t mp3_get_checksum (uint8_t *thebuf);
//static void send_func ();
void mp3_fill_checksum ();

uint8_t send_buf[] = {
    0x7E, 0xFF, 0x06, 00, 00, 00, 00, 00, 00, 0xEF};

volatile struct {
  uint8_t dataByte, bitsLeft,
          pin, done;
} txData = {0, 0, 0, 0};

void send_func () {
    for (uint8_t i=0; i<10; i++)
    {
        sendBySerial(send_buf[i]);
        while (!txData.done);
        _delay_ms(1);
    }
    sendBySerial(0);
    while (!txData.done);
}

static void fill_uint16_bigend (uint8_t *thebuf, uint16_t data) {
        *thebuf =       (uint8_t)(data>>8);
        *(thebuf+1) =   (uint8_t)data;
}


//calc checksum (1~6 byte)
uint16_t mp3_get_checksum (uint8_t *thebuf) {
    uint16_t sum = 0;
    for (int i=1; i<7; i++) {
            sum += thebuf[i];
    }
    return -sum;
}

//fill checksum to send_buf (7~8 byte)
void mp3_fill_checksum () {
        uint16_t checksum = mp3_get_checksum (send_buf);
        fill_uint16_bigend (send_buf+7, checksum);
}

void mp3_send_cmd (uint8_t cmd) {
    send_buf[0] = 0x7E;
    send_buf[3] = cmd;
    fill_uint16_bigend ((send_buf+5), 0);
    mp3_fill_checksum ();
    send_func ();
}

static void mp3_send_cmd_2 (uint8_t cmd, uint16_t arg) {
    send_buf[0] = 0x7E;
    send_buf[3] = cmd;
    fill_uint16_bigend ((send_buf+5), arg);
    mp3_fill_checksum ();
    send_func ();
}

//0x06 set volume 0-30
void mp3_set_volume (uint16_t volume) {
    mp3_send_cmd_2 (0x06, volume);
}


void mp3_set_EQ (uint16_t eq)
{
    mp3_send_cmd_2 (0x07, eq);
}

void mp3_set_folder (uint16_t folder)
{
    mp3_send_cmd_2 (0x0F, (folder<<8));

}

void mp3_play () {
        mp3_send_cmd (0x0d);
}

//
void mp3_play_physical (uint16_t num) {
    mp3_send_cmd_2 (0x03, num);
}

// Play a numbered track in the mp3 filder
void mp3_play_in_mp3 (uint16_t num) {
    mp3_send_cmd_2 (0x12, num);
}


//
void mp3_random_play () {
    mp3_send_cmd (0x18);
}

//play mp3 files in a folder in your tf card
//7E FF 06 17 00 00 02 FE E2 EF
void mp3_repeat_play_folder (uint16_t folder) {
    mp3_send_cmd_2 (0x17, folder);
}
//
void mp3_repeat_play_track (uint16_t track)
{
    mp3_send_cmd_2 (0x08, track);
}

void start_repeat()
{
    mp3_send_cmd_2 (0x19, 0x00);
}

void stop_repeat()
{
    mp3_send_cmd_2 (0x19, 0x01);
}

void mp3_next () {
    mp3_send_cmd (0x01);
}
void mp3_previous () {
    mp3_send_cmd (0x02);
}

void mp3_folder_next (uint8_t folder)
{
     mp3_send_cmd_2 (0x01, folder);
}

void mp3_stop ()
{
    mp3_send_cmd (0x16);
}


void mp3_reset () {
    mp3_send_cmd (0x0c);
}


//---------------------------------------------------------
void sendBySerial(const uint8_t data) {
  _delay_ms(10);
  txData.dataByte = data;
  txData.bitsLeft = 10;
  txData.done = 0;
  // Reset counter
  TCNT0 = 0;
  // Activate timer0 A match interrupt
  TIMSK = 1 << OCIE0A;

} // sendBySerial

//---------------------------------------------------------
// Blocking
void sendStrBySerial(char *p) {

  while (*p != '\0') {
    sendBySerial(*p++);
    while (!txData.done);
  } // while
  _delay_ms(1);

} // sendStrBySerial

//---------------------------------------------------------
void initSerial(const uint8_t portPin) {
    txData.pin = portPin;
    // Define pin as output
    DDRB |= (1 << txData.pin);
    // Set it to the default HIGH
    PORTB |= (1 << txData.pin);
    TCCR0A = 0;  TCCR0B = 0;// Clear all settings
    // Set top value for counter 0 F_CPU
    OCR0A = 104; // 104 Assumes nothing.
    #if F_CPU == 1000000
    TCCR0B = _BV(CS00);  // Clear all settings
    #elif  F_CPU == 8000000
    TCCR0B = _BV(CS01);
    #endif
    TIMSK = 0;   // Disable all timer interrupts
    // No A/B match output; just CTC mode
    TCCR0A = (1 << WGM01);
} // initSerial;


//---------------------------------------------------------
// Timer 0 A-match interrupt
ISR(TIMER0_COMPA_vect) {

  uint8_t bitVal;

  switch (txData.bitsLeft) {

    case 10: bitVal = 0; break;
    case  1: bitVal = 1; break;
    case  0: TIMSK &= ~(1 << OCIE0A);
             txData.done = 1;
             return;

    default:
      bitVal = txData.dataByte & 1;
      txData.dataByte >>= 1;

  } // switch

  if (bitVal) PORTB |= (1 << txData.pin);
   else PORTB &= ~(1 << txData.pin);
  --txData.bitsLeft;

}

/*******************************************************************************
Get track count by trying to play tracks sequentially and testing if the busy
pin goes low for a time. If it does the track exists, if not then that's trackcount +1
This only needs to run once on bootup. Although a SDCard change may be done
while powered up this is unlikely to be an issue 'in the field'.
If there's no Rx serial pin this will work even though it's a little clunky.
WARNING: This doesn't skip tracks so make sure they start at 0001.mp3 and are serially
numbered.
WARNING: If a watchdog is enabled it will timeout and reset the controller. Add WDT disable stuff.
@return uint8_t Track count.
*******************************************************************************/
uint8_t get_file_count()
{
    uint8_t num = 1;
    uint8_t n;
    num = 1;
    uint8_t wdtcr = WDTCR;
    wdt_disable();

    for(n = 1; n < 10; n++) // Arbitary limit
    {
        initSerial(OUT_A); _delay_ms(1);
        mp3_set_volume(0);
        mp3_play_in_mp3 (num);
        DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
        //initSystemClock();
        _delay_ms(200); // Just long enough to allow the player to start.
        // Test if it's high, if it is then the track hasn't played and doesn't exist.
        if(PIN_PORT & (1 << OUT_A))
        {
            initSerial(OUT_A);
            mp3_stop();
            continue;
        } //Try again
        else
        {
            initSerial(OUT_A);
            mp3_stop();
            initSystemClock();
            _delay_ms(1);
            num++;
        }
    }

    // Re-enable the watchdog
    WDTCR = wdtcr;
    return num - 1;
}

/**
Play a specified frack in the mp3 folder
@param uint8_t track_num
@param uint8_t volume
*/
void play_track(uint8_t track_num, uint8_t volume)
{
    // Save the counter
    uint16_t count = TCNT0;
    //uint32_t timer;
    initSerial(OUT_A); _delay_ms(1);
    mp3_set_volume(volume);
    mp3_play_in_mp3 (track_num);
    DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(1);
    initSystemClock(); // So we can add a timeout.
    TCNT0 = count;
    //timer = millis();
}


void set_volume(uint8_t volume)
{
    uint16_t count = TCNT0;
    initSerial(OUT_A); _delay_ms(1);
    mp3_set_volume(volume);
    DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(1);
    initSystemClock(); // So we can add a timeout.
    TCNT0 = count;
}


void reset_mp3()
{
uint16_t count = TCNT0;
    initSerial(OUT_A); _delay_ms(1);
    mp3_reset();
    DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(1);
    initSystemClock(); // So we can add a timeout.
    TCNT0 = count;
}
