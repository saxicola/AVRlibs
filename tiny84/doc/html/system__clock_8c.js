var system__clock_8c =
[
    [ "clockCyclesPerMicrosecond", "system__clock_8c.html#ae6741cdb6d1d3f17299b9874ddc2c12e", null ],
    [ "clockCyclesToMicroseconds", "system__clock_8c.html#a262d28355fa4b64b12743b5e93f7516b", null ],
    [ "microsecondsToClockCycles", "system__clock_8c.html#aa4a5bbc71d71ab25856c1366b6ca15bb", null ],
    [ "initSystemClock", "system__clock_8c.html#a81308abf4a2f57e7b17ce1c8e90d4a51", null ],
    [ "ISR", "system__clock_8c.html#aa51babb09e7bd89a47c6addbbc02ca3e", null ],
    [ "micros", "system__clock_8c.html#a2b97cb2cd2661deade2d301d38c4ebe8", null ],
    [ "millis", "system__clock_8c.html#adb94fbd930038e1510574dd4bcf07fe1", null ],
    [ "kFractInc", "system__clock_8c.html#a105bb53f721cef90263acb852a8a090a", null ],
    [ "kFractMax", "system__clock_8c.html#a6b98e31933b3f1a2493ecea1d6a85b23", null ],
    [ "kMicrosecondsPerOverflowTIMER0", "system__clock_8c.html#a5f4be18de217f30f5303dfef34b5f9d8", null ],
    [ "kMillisInc", "system__clock_8c.html#ac9fb69b8a4039448fc27d36d9b43eec1", null ],
    [ "timer0_fract", "system__clock_8c.html#ac131a9685d8672a515d14c8c5ad63d7f", null ],
    [ "timer0_millis", "system__clock_8c.html#ab34d3974411fdee7da8c6b4f650502e9", null ],
    [ "timer0_overflow_count", "system__clock_8c.html#a79a4c8c254cfb57c6ac3393a396c7a4f", null ]
];