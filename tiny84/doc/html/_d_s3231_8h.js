var _d_s3231_8h =
[
    [ "DStm", "struct_d_stm.html", "struct_d_stm" ],
    [ "DS3231_ADR", "_d_s3231_8h.html#a8f136326d0ae87ebcc16d3871afb9f56", null ],
    [ "DS3231_get_alarm", "_d_s3231_8h.html#a6844ee91195b6ea9b34af4143c62d957", null ],
    [ "DS3231_get_temp", "_d_s3231_8h.html#ae181b5240f93f814aec334647e491e98", null ],
    [ "DS3231_get_time", "_d_s3231_8h.html#a1180c4b25d2ec42b74fd820b423e1a08", null ],
    [ "DS3231_init", "_d_s3231_8h.html#a7efda15884dcf2bfdee5c4f5df0d0180", null ],
    [ "DS3231_read", "_d_s3231_8h.html#a29d4c7f415994ee9b772ad5e0bd78220", null ],
    [ "DS3231_set_alarm", "_d_s3231_8h.html#a818f832e52ab4132221233e3b92dd245", null ],
    [ "DS3231_set_time", "_d_s3231_8h.html#a7b41ca9dbc61bd7af597c812cc122743", null ],
    [ "DS3231_start_osc", "_d_s3231_8h.html#ad4d1ad7ae41842a68547cb5720dc56ac", null ],
    [ "DS3231_write", "_d_s3231_8h.html#a94f689e4619af588d6aa5e3fb7bcc848", null ],
    [ "DS3231_zero_seconds", "_d_s3231_8h.html#a6baaacf8377930e5c4bd18b86dcd7617", null ],
    [ "_tm", "_d_s3231_8h.html#ae4541492968b1117b78ebbd5b1161450", null ]
];