var searchData=
[
  ['databyte',['dataByte',['../df__player_8c.html#a26b9b3770125a4f30f2181b30ba8f333',1,'df_player.c']]],
  ['ddr_5fport',['DDR_PORT',['../df__player_8h.html#a2c9d21577f173d0a9154f858332556b1',1,'df_player.h']]],
  ['dec2bcd',['dec2bcd',['../_d_s3231_8c.html#a987db7f91ddfe0193d2bde11ed32debe',1,'DS3231.c']]],
  ['df_5fplayer_2ec',['df_player.c',['../df__player_8c.html',1,'']]],
  ['df_5fplayer_2eh',['df_player.h',['../df__player_8h.html',1,'']]],
  ['done',['done',['../df__player_8c.html#a4a3da89cf9b53baa4e27e9b8d2a8491d',1,'df_player.c']]],
  ['ds3231_2ec',['DS3231.c',['../_d_s3231_8c.html',1,'']]],
  ['ds3231_2eh',['DS3231.h',['../_d_s3231_8h.html',1,'']]],
  ['ds3231_5fadr',['DS3231_ADR',['../_d_s3231_8h.html#a8f136326d0ae87ebcc16d3871afb9f56',1,'DS3231.h']]],
  ['ds3231_5fget_5falarm',['DS3231_get_alarm',['../_d_s3231_8h.html#a6844ee91195b6ea9b34af4143c62d957',1,'DS3231.h']]],
  ['ds3231_5fget_5ftemp',['DS3231_get_temp',['../_d_s3231_8c.html#ae181b5240f93f814aec334647e491e98',1,'DS3231_get_temp():&#160;DS3231.c'],['../_d_s3231_8h.html#ae181b5240f93f814aec334647e491e98',1,'DS3231_get_temp():&#160;DS3231.c']]],
  ['ds3231_5fget_5ftime',['DS3231_get_time',['../_d_s3231_8c.html#a264084cfd200d5508d72607f5bb6a457',1,'DS3231_get_time(struct DStm *_tm):&#160;DS3231.c'],['../_d_s3231_8h.html#a1180c4b25d2ec42b74fd820b423e1a08',1,'DS3231_get_time(struct DStm *):&#160;DS3231.c']]],
  ['ds3231_5finit',['DS3231_init',['../_d_s3231_8c.html#a1db57b059c5a697cf67f2a426ea5471c',1,'DS3231_init():&#160;DS3231.c'],['../_d_s3231_8h.html#a7efda15884dcf2bfdee5c4f5df0d0180',1,'DS3231_init(void):&#160;DS3231.c']]],
  ['ds3231_5fread',['DS3231_read',['../_d_s3231_8c.html#a29d4c7f415994ee9b772ad5e0bd78220',1,'DS3231_read(uint8_t address, uint8_t *data):&#160;DS3231.c'],['../_d_s3231_8h.html#a29d4c7f415994ee9b772ad5e0bd78220',1,'DS3231_read(uint8_t address, uint8_t *data):&#160;DS3231.c']]],
  ['ds3231_5fset_5falarm',['DS3231_set_alarm',['../_d_s3231_8h.html#a818f832e52ab4132221233e3b92dd245',1,'DS3231.h']]],
  ['ds3231_5fset_5ftime',['DS3231_set_time',['../_d_s3231_8c.html#a2e4a0a2030901ecbc52e347dd8ed0647',1,'DS3231_set_time(struct DStm *tm_):&#160;DS3231.c'],['../_d_s3231_8h.html#a7b41ca9dbc61bd7af597c812cc122743',1,'DS3231_set_time(struct DStm *):&#160;DS3231.c']]],
  ['ds3231_5fstart_5fosc',['DS3231_start_osc',['../_d_s3231_8c.html#ad4d1ad7ae41842a68547cb5720dc56ac',1,'DS3231_start_osc(void):&#160;DS3231.c'],['../_d_s3231_8h.html#ad4d1ad7ae41842a68547cb5720dc56ac',1,'DS3231_start_osc(void):&#160;DS3231.c']]],
  ['ds3231_5fwrite',['DS3231_write',['../_d_s3231_8c.html#a94f689e4619af588d6aa5e3fb7bcc848',1,'DS3231_write(uint8_t address, uint8_t data):&#160;DS3231.c'],['../_d_s3231_8h.html#a94f689e4619af588d6aa5e3fb7bcc848',1,'DS3231_write(uint8_t address, uint8_t data):&#160;DS3231.c']]],
  ['ds3231_5fzero_5fseconds',['DS3231_zero_seconds',['../_d_s3231_8c.html#a6baaacf8377930e5c4bd18b86dcd7617',1,'DS3231_zero_seconds(void):&#160;DS3231.c'],['../_d_s3231_8h.html#a6baaacf8377930e5c4bd18b86dcd7617',1,'DS3231_zero_seconds(void):&#160;DS3231.c']]],
  ['dstm',['DStm',['../struct_d_stm.html',1,'']]]
];
