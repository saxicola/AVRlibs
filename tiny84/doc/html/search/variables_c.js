var searchData=
[
  ['s_5fnow',['s_now',['../timenow_8h.html#aaeca69b5f335fc42399d581beac8b3a7',1,'timenow.h']]],
  ['sec',['sec',['../struct_d_stm.html#a90c2ace84e5523d06b7162ea5928acc1',1,'DStm']]],
  ['send_5fbuf',['send_buf',['../df__player_8c.html#aeb3da647450f594cb5d21bc77d89e2fb',1,'df_player.c']]],
  ['serialasmtx',['SerialAsmTx',['../_asm_tiny_serial_8h.html#ad74aba2a0d176a08ce7c25d9a478a968',1,'AsmTinySerial.h']]],
  ['stp16_5fclkpin',['stp16_clkPin',['../stp16cp05_8h.html#a622b48e1e702677e9012ed2bf5343f08',1,'stp16cp05.h']]],
  ['stp16_5fddr',['stp16_ddr',['../stp16cp05_8h.html#ae11af0930b71a1dc2426357db7ce331b',1,'stp16cp05.h']]],
  ['stp16_5flepin',['stp16_lePin',['../stp16cp05_8h.html#a4f1dee7a0e52822d1e6088ec7206b829',1,'stp16cp05.h']]],
  ['stp16_5foepin',['stp16_oePin',['../stp16cp05_8h.html#a2834e77a3662149b271b90444430038d',1,'stp16cp05.h']]],
  ['stp16_5fport',['stp16_port',['../stp16cp05_8h.html#ab2c8f5a4df0a927de9d5f67cd59460b1',1,'stp16cp05.h']]],
  ['stp16_5fsdipin',['stp16_sdiPin',['../stp16cp05_8h.html#a030685c085ab76e68011aa13ae6af5b0',1,'stp16cp05.h']]]
];
