var indexSectionsWithContent =
{
  0: "_abcdefghiklmoprstwy",
  1: "d",
  2: "abdgist",
  3: "abcdgilmps",
  4: "_abdefghkmopstwy",
  5: "abcdegilmoprst",
  6: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Modules"
};

