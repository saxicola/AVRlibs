var _d_s3231_8c =
[
    [ "bcd2dec", "_d_s3231_8c.html#a2a7f23c9884f6a822b74cbd40c2f0594", null ],
    [ "dec2bcd", "_d_s3231_8c.html#a987db7f91ddfe0193d2bde11ed32debe", null ],
    [ "DS3231_get_temp", "_d_s3231_8c.html#ae181b5240f93f814aec334647e491e98", null ],
    [ "DS3231_get_time", "_d_s3231_8c.html#a264084cfd200d5508d72607f5bb6a457", null ],
    [ "DS3231_init", "_d_s3231_8c.html#a1db57b059c5a697cf67f2a426ea5471c", null ],
    [ "DS3231_read", "_d_s3231_8c.html#a29d4c7f415994ee9b772ad5e0bd78220", null ],
    [ "DS3231_set_time", "_d_s3231_8c.html#a2e4a0a2030901ecbc52e347dd8ed0647", null ],
    [ "DS3231_start_osc", "_d_s3231_8c.html#ad4d1ad7ae41842a68547cb5720dc56ac", null ],
    [ "DS3231_write", "_d_s3231_8c.html#a94f689e4619af588d6aa5e3fb7bcc848", null ],
    [ "DS3231_zero_seconds", "_d_s3231_8c.html#a6baaacf8377930e5c4bd18b86dcd7617", null ],
    [ "_tm", "_d_s3231_8c.html#ae4541492968b1117b78ebbd5b1161450", null ]
];