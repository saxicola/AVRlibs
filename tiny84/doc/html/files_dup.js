var files_dup =
[
    [ "adc.c", "adc_8c.html", "adc_8c" ],
    [ "adc.h", "adc_8h.html", "adc_8h" ],
    [ "AsmTinySerial.h", "_asm_tiny_serial_8h.html", "_asm_tiny_serial_8h" ],
    [ "bcdtest.c", "bcdtest_8c.html", "bcdtest_8c" ],
    [ "df_player.c", "df__player_8c.html", "df__player_8c" ],
    [ "df_player.h", "df__player_8h.html", "df__player_8h" ],
    [ "DS3231.c", "_d_s3231_8c.html", "_d_s3231_8c" ],
    [ "DS3231.h", "_d_s3231_8h.html", "_d_s3231_8h" ],
    [ "gitversion.h", "gitversion_8h.html", "gitversion_8h" ],
    [ "i2c_lcd.c", "i2c__lcd_8c.html", "i2c__lcd_8c" ],
    [ "i2c_lcd.h", "i2c__lcd_8h.html", "i2c__lcd_8h" ],
    [ "i2cmaster.h", "i2cmaster_8h.html", "i2cmaster_8h" ],
    [ "softuart.c", "softuart_8c.html", "softuart_8c" ],
    [ "softuart.h", "softuart_8h.html", "softuart_8h" ],
    [ "stp16cp05.c", "stp16cp05_8c.html", "stp16cp05_8c" ],
    [ "stp16cp05.h", "stp16cp05_8h.html", "stp16cp05_8h" ],
    [ "sunsets.h", "sunsets_8h.html", "sunsets_8h" ],
    [ "system_clock.c", "system__clock_8c.html", "system__clock_8c" ],
    [ "system_clock.h", "system__clock_8h.html", "system__clock_8h" ],
    [ "timenow.h", "timenow_8h.html", "timenow_8h" ]
];