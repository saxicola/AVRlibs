
#ifndef NEXUS_H
#define NEXUS_H





void nexus_send(uint8_t id, int temperature, uint16_t humidity, int status);
uint64_t _parse(uint8_t id, int battery, int channel, int temp, uint16_t hum);

#endif
