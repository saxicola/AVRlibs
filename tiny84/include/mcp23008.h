

#include <stdio.h>
#include <avr/io.h>


#ifndef MC23008_H
#define MC23008_H

#define MCP23008_ADDR	 0x27
#define MCP23008_IODIR 	 0x00
#define MCP23008_IPOL 	 0x01
#define MCP23008_GPINTEN 0x02
#define MCP23008_DEFVAL  0x03
#define MCP23008_INTCON  0x04
#define MCP23008_IOCON   0x05
#define MCP23008_GPPU 	 0x06
#define MCP23008_INTCAP  0x08
#define MCP23008_INTF  	 0x07
#define MCP23008_GPIO  	 0x09
#define MCP23008_OLAT  	 0x0A


/* From the python file

    ''' Set interrupt enable on the pins that need it.'''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_GPINTEN, inten)

    def set_defval(self, defval):
        ''' Enable comparison withe the default values on the selected pins.
        If enabled, a value opposite to that in the default value register will cause
        an interrupt to occur.  See also GPINTEN and INTCON registers.'''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_DEFVAL, defval)

    def set_intcon(self, intcon):
        ''' Enable interrupts for comparing to values in the DEFVAL register on the selected pins.'''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_INTCON, intcon)

    def set_iodir(self, iodir):
        ''' Set IO direction for each pin. 1 = input, 0 = output. '''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_IODIR, iodir)

    def set_pullups(self, pullups):
        ''' Enable 10K pullups on the selected pins. 1 = pullup enabled.'''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_GPPU, pullups)

    def set_input_polarity(self, ipol):
        ''' Reverse the input polarity on the selected pins. '''
        self.bus.write_byte_data(self.MCP23008_addr, self.MCP23008_IPOL, ipol)


	def read(self):
        '''
        Read and print out the pin status
        '''
        pins = self.bus.read_byte_data(self.MCP23008_addr, self.MCP23008_GPIO)
        return pins


*/


// Set IO direction for each pin. 1 = input, 0 = output.
void mcp23008_set_iodir(uint8_t pins);

// Write the pins value to the pins for output
void mcp23008_write(uint8_t pins);

// Read the values on the pins.
void mcp23008_read(uint8_t * pins);


#endif
