
#ifndef _I2CKAYPAD_H
#define _I2CKEYPAD_H


//char key_read_i2c(void);
char key_write_i2c(unsigned char data);
uint8_t read_pad(uint8_t rows);
uint8_t i2c_keypad_detect(void);

#endif
