/*
The basis of this code came from:
somewhere on the WWW
Fixed to work correctly by mikee@saxicola.co.uk
Added other functions to get and set.
Tested with ATtiny24.
*/
#include "DS3231.h"
#include "i2cmaster.h"
#include <stdbool.h>
#include <util/delay.h>

// statically allocated structure for time value
struct DStm _tm;

/*
Make sure the oscillator is running.
*/
uint8_t DS3231_start_osc(void)
{
    i2c_start_wait((DS3231_ADR << 1) | I2C_WRITE);     // set device address and write mode
    i2c_write(0x0E);
    i2c_write(0x00); // Everything can be 0 but /EOSC =0 starts the oscillator.
    i2c_stop ();
}



uint8_t dec2bcd (uint8_t d)
{
    return ((d / 10 * 16) + (d % 10));
}

uint8_t
bcd2dec (uint8_t b)
{
    return ((b / 16 * 10) + (b % 16));
}


/*
@param time struct
@return int value depends on success or failure 0 = No errors.
*/
uint8_t DS3231_set_time(struct DStm* tm_)
{
    uint8_t ret = 0;
    uint8_t century;
    if (tm_->year >= 2000) {
        century = 0x80;
        tm_->year = tm_->year - 2000;
    } else {
        century = 0;
        tm_->year = tm_->year - 1900;
    }
    i2c_start_wait((DS3231_ADR << 1) | I2C_WRITE);     // set device address and write mode
    i2c_write(0x00);                        // write register start address = 0
    i2c_write(0x80);                        // Reset the seconds to zero avoid race conditions
    ret += i2c_write(dec2bcd(tm_->min));    // minutes
    ret += i2c_write(dec2bcd(tm_->hour));   // hours
    ret += i2c_write(dec2bcd(tm_->wday));   // day of the week
    ret += i2c_write(dec2bcd(tm_->mday));   // day of the month
    ret += i2c_write(dec2bcd(tm_->mon) + century); // month + century bit
    ret += i2c_write(dec2bcd(tm_->year));   // year
    i2c_stop ();
    // We have to restart in order to set the seconds and restart the clock
    i2c_start_wait((DS3231_ADR << 1) | I2C_WRITE);     // set device address and write mode
    i2c_write(0x00);                        // write register start address = 0
    _delay_ms(2);
    ret += i2c_write(dec2bcd(tm_->sec)); // Seconds last as this restarts the clock
    // set stop conditon = release bus
    i2c_stop ();
    return ret;
}


/*
Set the seconds register to zero.
This is a test function, we really shouldn't need this in production.
*/
void DS3231_zero_seconds(void)
{
  i2c_start_wait((DS3231_ADR << 1) | I2C_WRITE);
  i2c_write(0x00);
  i2c_write(0x00);
  i2c_stop();
}

/*
Fill in the time struct with RTC data
@param void
@return struct with all the time fields.
*/
uint8_t DS3231_get_time (struct DStm * _tm)
{
    //uint8_t dat;
    uint8_t rtc[9] = {0};
    uint8_t century = 0;

    i2c_start_wait((DS3231_ADR << 1) | I2C_WRITE);

    if (i2c_write (0x00))
    {
        i2c_stop ();
        return 1;
    }

    i2c_rep_start ((DS3231_ADR << 1) | I2C_READ);
    for (uint8_t i = 0; i < 7; i++)
    {
        rtc[i] = i2c_readAck ();
    }
    i2c_readNak ();
    i2c_stop ();

    _tm->sec = bcd2dec (rtc[0]); // Seconds
    _tm->min = bcd2dec (rtc[1]);  // Minutes
    // 24 or 48 hour clock?
    if(_tm->hour & 0b01000000) // is it 12 hour clock?
        {
            _tm->hour = bcd2dec (rtc[2] & 0b00011111);
            if (_tm->hour < 12 || _tm->hour == 0 )
                _tm->am = 1;
            else
                _tm->am = 1;
        }
    else
    _tm->hour = bcd2dec (rtc[2] & 0b00111111); // Hour
    _tm->wday = bcd2dec (rtc[3]); // Week day
    _tm->mday = bcd2dec (rtc[4]); // Day of Month
    _tm->mon = bcd2dec (rtc[5] & 0x1F);  // Month, returns 1-12
    century = (rtc[5] & 0x80) >> 7;
    _tm->year = century == 1 ? 2000 + bcd2dec (rtc[6]) : 1900 + bcd2dec (rtc[6]);    // year 0-99
    _tm->wday = bcd2dec (rtc[3]);    // returns 1-7
    return 0;
}

// Function to read RTC
uint8_t
DS3231_read (uint8_t address, uint8_t * data)
{
    uint8_t ret;

    ret = i2c_start ((DS3231_ADR << 1) + I2C_WRITE);   // Issue start on I2C bus in write mode

    if (ret == 1)
      {
      return false;
      }

    ret = i2c_write (address);  // send the address of data to read

    if (ret == 1)
      {
      return false;
      }

    i2c_rep_start ((DS3231_ADR << 1) + I2C_READ);  //Re-issue Start but read this time

    if (ret == 1)
      {
      return false;
      }

    *data = i2c_readNak (); // read byte

    i2c_stop ();        // Issue a stop on I2C bus

    return true;
}

// Function to write to RTC

uint8_t
DS3231_write (uint8_t address, uint8_t data)
{
    uint8_t ret;

    ret = i2c_start ((DS3231_ADR << 1) + I2C_WRITE);   // Issue start on I2C bus in write mode

    if (ret == 1)
      {
      return false;
      }

    ret = i2c_write (address);  // send the address of data to write

    if (ret == 1)
      {
      return false;
      }

    ret = i2c_write (data); // send the data

    if (ret == 1)
      {
      return false;
      }

    i2c_stop ();        // Issue a stop on I2C bus

    return 1;
}

// Read temperature registers and convert to int
float
DS3231_get_temp ()
{
    uint8_t i, whole, data, frac;
    float tempF;

    // set flag
    i = 0;

    // Read whole part of temperature
    DS3231_read (0x11, &data);
    whole = data;

    // Read Fractional part of temperature
    DS3231_read (0x12, &data);
    frac = data >> 6;

    // Reading for whole is in two's complement so if it is negative, convert.
    if (whole & (1 << 7))
      {
      // xor with 0b11111111 to get complement then add 1
      whole ^= 0xFF;
      whole++;
      // set flag to indicate temp is negative
      i = 1;
      }
    // Combine whole and fractional parts.
    tempF = ((whole) + (frac * .25));

    // Make negative if needed
    if (i == 1)
    tempF *= -1;

    return tempF;
}

//  Function to Clear the CH bit and set 12 Hr Mode

void
DS3231_init ()
{

    uint8_t temp;
    DS3231_read (0x00, &temp);  // Read Seconds Register
    temp &= (~(0x80));    //Clear CH Bit
    DS3231_write (0x00, temp);  // Write back to RTC
    DS3231_read (0x02, &temp);  //Read Hour Register
    temp |= (0b00000000);  //UnSet 12Hour BIT
    DS3231_write (0x02, temp);  //Write Back to RTC
}
