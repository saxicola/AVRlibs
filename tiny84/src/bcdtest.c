

#include <stdint.h>
#include <stdio.h>
#include <stddef.h>



static int dec2bcd (int d);


static int dec2bcd (int d)
{
    return ((d / 10 * 16) + (d % 10));
}



int main (void)
{
    int h = 11;
    int bcd = 0;
    bcd = dec2bcd (h);
    printf("bcd =  %x\n\n", bcd);
}
