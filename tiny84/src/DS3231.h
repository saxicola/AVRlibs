/*
The basis of this code came from:
somewhere on the WWW
and didn't work as was.
Fixed to work correctly by mikee@saxicola.co.uk
Added other functions to get and set.
*/
#ifndef DS3231_H
#define DS3231_H

#define DS3231_ADR 0b1101000
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

enum ds3231_registers {
  DS3231_REGISTER_SECONDS,
  DS3231_REGISTER_MINUTES,
  DS3231_REGISTER_HOURS,
  DS3231_REGISTER_DAY_OF_WEEK,
  DS3231_REGISTER_DATE,
  DS3231_REGISTER_MONTH,
  DS3231_REGISTER_YEAR,
  DS3231_REGISTER_ALARM1_SECONDS,
  DS3231_REGISTER_ALARM1_MINUTES,
  DS3231_REGISTER_ALARM1_HOURS,
  DS3231_REGISTER_ALARM1_DAY_OF_WEEK_OR_DATE,
  DS3231_REGISTER_ALARM2_MINUTES,
  DS3231_REGISTER_ALARM2_HOURS,
  DS3231_REGISTER_ALARM2_DAY_OF_WEEK_OR_DATE,
  DS3231_REGISTER_CONTROL,
  DS3231_REGISTER_CONTROL_STATUS,
  DS3231_REGISTER_AGING_OFFSET,
  DS3231_REGISTER_ALARM2_TEMP_MSB,
  DS3231_REGISTER_ALARM2_TEMP_LSB
  };

struct DStm {
    uint8_t sec;      // 0 to 59
    uint8_t min;      // 0 to 59
    uint8_t hour;     // 0 to 23
    uint8_t wday;     // 1-7
    uint8_t mday;     // 1 to 31
    uint8_t mon;      // 1 to 12
    uint16_t year;    // year-99


    // 12-hour clock data
    bool am; // true for AM, false for PM
    uint8_t twelveHour; // 12 hour clock time
};

// statically allocated
extern struct DStm _tm;
void DS3231_init(void);
uint8_t DS3231_get_time(struct DStm *);
uint8_t DS3231_set_time(struct DStm *);
uint8_t DS3231_set_alarm(struct DStm * );
struct DStm*  DS3231_get_alarm(void);
float DS3231_get_temp();
uint8_t DS3231_write(uint8_t address,uint8_t data);
uint8_t DS3231_read(uint8_t address,uint8_t *data);
uint8_t DS3231_start_osc(void);
void DS3231_zero_seconds(void);
#endif
