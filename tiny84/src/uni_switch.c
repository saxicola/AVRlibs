/*
 * uni_switch.c
 *
 * Copyright 2020 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * The goal of this module is to detect what's attached to the "switch" pins and enable the approriate
 code.
 This is the goal.
 I use, mostly ultrasonic HC-04, PIR detectors and a regular momentary switch.  I'd like them to be
 interchangeable, in the field, without having to edit the code.

 */

 #include "uni_switch.h"



/*
Detect the switch and return something.
A swtic, by it's nature as passive device cannot be detected but, an ultrasonic responds to a trigger
pulse and a PIR will pull the switch line high when it is present.

Ultrasonic:
    Test echo line
    Send trigger pulse
    Check echo line again after appropriate delay.
PIR:
    Test trigger line
Switch:
    Assume a switch if the above tests fail.

Call with: set_bits_func_correct (&PORTB, 0x55); Obviously change the values.
@param Pointer to the PORT
@param uint8_t
@return Something, probably an enum.

*/
uint8_t detect_switch(volatile uint8_t *port, uint8_t mask)
{
    enum trigger trig;
    trig = button; // Default if we detect nothing.


    return trig;
}
