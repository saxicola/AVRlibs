/*
 * i2c_keypad.c
 *
 * Copyright 2018 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
  I2C interface for a 4x4 matrix keypad.
  Sample each row in turn and report any pressed keys. Double key presses are
  treated are errors.

  We can use an interrupt routine to call this.
  The I2C I/O expander provides an open-drain output that goes low whenever one
  of the I/O lines changes state. This capability can be used notify the micro of
  keyboard activity by setting all of the row lines low when not actively
  scanning the keypad. Then, when any key is pressed the corresponding column
  line will go low causing a high-to-low transition on the /INT output of the
  I/O expander. If this output is connected to one of the external interrupt pins
  we can poll the keybord when the ISR is triggered.

 */
#include "i2cmaster.h"
#include "i2c_keypad.h"
#include <util/delay.h>
#include <avr/pgmspace.h>


#define KEYPAD_ADDR    0x38 // No jumpers.

static uint8_t read_col(uint8_t row);


/*******************************************************************************
Detect presence of the keypad.
*/
uint8_t i2c_keypad_detect()
{

}

/*******************************************************************************
 * Sample the keypad row and return the byte.
 * @param uint8_t Row to read
 */
static uint8_t read_col(uint8_t row)
{
    i2c_init();
    uint16_t retval = 0;
    if(i2c_start((KEYPAD_ADDR << 1) | I2C_WRITE) == 1) return 255;
    i2c_write((0xff) &~(1 << row));
    i2c_stop();
    _delay_ms(2);
    i2c_start_wait((KEYPAD_ADDR << 1) | I2C_READ);
    retval = i2c_readNak();
    i2c_stop();
    return ~(retval  >> 4); // Cols
}

/*******************************************************************************
 * Sample the keypad row-by-row and return the key value.
 * @param uint8_t Number of rows in keyboard
 * @return uint8_t Coded value of a single key or 255 on error.
 */
uint8_t read_pad(uint8_t rows)
{
    uint8_t keynum = 0;
    uint8_t valid = 0;
    uint8_t row = 0;
    if(read_col(0) == 255) return 255;
    for (row = 0; row < rows; row++)
    {
        keynum = (read_col(row)) & 0x0f;
        if(keynum != 0)
        {
            return keynum + (row*8);

        }
    }
    return 0;
}

/*******************************************************************************

*/
char key_write_i2c(unsigned char data)
{
    unsigned char retval = 0;
    retval = i2c_start((KEYPAD_ADDR << 1) | I2C_WRITE);
    i2c_write(data);
    i2c_stop();
    return retval;
}
