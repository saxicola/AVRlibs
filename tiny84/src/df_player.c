/*
 * df_player.c
 *
 * Copyright 2017 Mike Evans <mikee@saxicola.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Functions for controlling a df_player mini.
 */



#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "df_player.h"
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/sfr_defs.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "system_clock.h"

static void fill_uint16_bigend (uint8_t *thebuf, uint16_t data);
static void save_regs(void);
static void restore_regs(void);
uint16_t mp3_get_checksum (uint8_t *thebuf);
//static void send_func ();
void mp3_fill_checksum ();

// Globals for storing and re-storing clobbered registers.
uint16_t tcnt0; // = TCNT0;
uint8_t ocr0a; //  = OCR0A;
uint8_t ocr1a; //  = OCR1A;
uint8_t tccr0a; // = TCCR0A;
uint8_t tccr0b; // = TCCR0B;
uint8_t timsk0; // = TIMSK0 = 0;   // Disable all timer interrupts




uint8_t send_buf[] = {
    0x7E, 0xFF, 0x06, 00, 00, 00, 00, 00, 00, 0xEF};

volatile struct {
  uint8_t dataByte, bitsLeft,
          pin, done;
} txData = {0, 0, 0, 0};


static void save_regs()
{
    tcnt0 = TCNT0;
    ocr0a  = OCR0A;
    ocr1a  = OCR1A;
    tccr0a  = TCCR0A;
    tccr0b = TCCR0B;
    timsk0 = TIMSK0;
}

static void restore_regs()
{
    TCNT0 = tcnt0;
    OCR0A = ocr0a;
    OCR1A = ocr1a;
    TCCR0A = tccr0a;
    TCCR0B = tccr0b;
    TIMSK0 = timsk0 ;
}


void send_func () {
    for (uint8_t i=0; i<10; i++)
    {
        sendBySerial(send_buf[i]);
        while (!txData.done);
        _delay_ms(1);
    }
    sendBySerial(0);
    while (!txData.done);
}

static void fill_uint16_bigend (uint8_t *thebuf, uint16_t data) {
        *thebuf =       (uint8_t)(data>>8);
        *(thebuf+1) =   (uint8_t)data;
}


//calc checksum (1~6 byte)
uint16_t mp3_get_checksum (uint8_t *thebuf) {
    uint16_t sum = 0;
    for (int i=1; i<7; i++) {
            sum += thebuf[i];
    }
    return -sum;
}

//fill checksum to send_buf (7~8 byte)
void mp3_fill_checksum () {
        uint16_t checksum = mp3_get_checksum (send_buf);
        fill_uint16_bigend (send_buf+7, checksum);
}

void mp3_send_cmd (uint8_t cmd) {
    send_buf[0] = 0x7E;
    send_buf[3] = cmd;
    fill_uint16_bigend ((send_buf+5), 0);
    mp3_fill_checksum ();
    send_func ();
}

static void mp3_send_cmd_2 (uint8_t cmd, uint16_t arg) {
    send_buf[0] = 0x7E;
    send_buf[3] = cmd;
    fill_uint16_bigend ((send_buf+5), arg);
    mp3_fill_checksum ();
    send_func ();
}

//0x06 set volume 0-30
void mp3_set_volume (uint16_t volume) {
    mp3_send_cmd_2 (0x06, volume);
}


void mp3_set_EQ (uint16_t eq)
{
    mp3_send_cmd_2 (0x07, eq);
}

void mp3_set_folder (uint16_t folder)
{
    mp3_send_cmd_2 (0x0F, (folder<<8));

}

void mp3_play () {
        mp3_send_cmd (0x0d);
}

//
void mp3_play_physical (uint16_t num) {
    mp3_send_cmd_2 (0x03, num);
}

// Play a numbered track in the mp3 filder
void mp3_play_in_mp3 (uint16_t num) {
    mp3_send_cmd_2 (0x12, num);
}


//
void mp3_random_play () {
    mp3_send_cmd (0x18);
}

//play mp3 files in a folder in your tf card
//7E FF 06 17 00 00 02 FE E2 EF
void mp3_repeat_play_folder (uint16_t folder) {
    mp3_send_cmd_2 (0x17, folder);
}
//
void mp3_repeat_play_track (uint16_t track)
{
    mp3_send_cmd_2 (0x08, track);
}

void start_repeat()
{
    mp3_send_cmd_2 (0x19, 0x00);
}

void stop_repeat()
{
    mp3_send_cmd_2 (0x19, 0x01);
}

void mp3_next () {
    mp3_send_cmd (0x01);
}
void mp3_previous () {
    mp3_send_cmd (0x02);
}

void mp3_folder_next (uint8_t folder)
{
     mp3_send_cmd_2 (0x01, folder);
}

void mp3_stop ()
{
    mp3_send_cmd (0x16);
}


void mp3_reset () {
    mp3_send_cmd (0x0c);
}


//---------------------------------------------------------
void sendBySerial(const uint8_t data) {
  _delay_ms(10);
  txData.dataByte = data;
  txData.bitsLeft = 10;
  txData.done = 0;
  // Reset counter
  TCNT0 = 0;
  // Activate timer0 A match interrupt
  TIMSK0 = 1 << OCIE0A;

} // sendBySerial

//---------------------------------------------------------
// Blocking
void sendStrBySerial(char *p) {

  while (*p != '\0') {
    sendBySerial(*p++);
    while (!txData.done);
  } // while
  _delay_ms(1);

} // sendStrBySerial

/******************************************************************************
Set up port for serial comms.
WARNING: This clobbers some of the timer registers so bare this in mind when
calling this routine.
*******************************************************************************/
void initSerial(const uint8_t portPin) {
    sei();
    txData.pin = portPin;
    // Define pin as output
    DDR_PORT |= (1 << txData.pin);
    // Set it to the default HIGH
    PORT |= (1 << txData.pin);

    TCCR0A = 0;  // Clear all settings
    TCCR0B = 0;  // Clear all settings
    TIMSK0 = 0;   // Disable all timer interrupts
    // Set top value for counter 0
    OCR0A = 104; // Assumes 8MHz INT OSC and prescalar = 8
    //OCR0A = 13; // Assumes 8MHz INT OSC and prescalar = 1
    // No A/B match output; just CTC mode
    TCCR0A = (1 << WGM01);
    // Set prescaler to clk/1
    TCCR0B =  1 << (CS00);

} // initSerial;


//---------------------------------------------------------
// Timer 0 A-match interrupt
ISR(TIM0_COMPA_vect) {

  uint8_t bitVal;

  switch (txData.bitsLeft) {

    case 10: bitVal = 0; break;
    case  1: bitVal = 1; break;
    case  0: TIMSK0 &= ~(1 << OCIE0A);
             txData.done = 1;
             return;

    default:
      bitVal = txData.dataByte & 1;
      txData.dataByte >>= 1;

  } // switch

  if (bitVal) PORT |= (1 << txData.pin);
   else PORT &= ~(1 << txData.pin);
  --txData.bitsLeft;

}



/*
Get track count by trying to play tracks sequentially and testing if the busy
pin goes low for a time. If it does the track exists, if not then that's trackcount +1
This only needs to run once on bootup. Although a SDCard change may be done
while powered up this is unlikely to be an issue 'in the field'.
If there's no Rx serial pin this will work even though it's a little clunky.
WARNING: This doesn't skip tracks so make sure they start at 0001.mp3 and are serially
numbered.
@return uint8_t Track count.
*/
uint8_t get_file_count()
{
    uint8_t num = 1;
    uint8_t n;
    num = 1;

    for(n = 9; n-- ; n) // Arbitary limit
    {
        initSerial(OUT_A); _delay_ms(10);
        mp3_set_volume(0);
        mp3_play_in_mp3 (num);
        DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
        //initSystemClock();
        _delay_ms(200); // Just long enough to allow the player to start.
        // Test if it's high, if it is then the track hasn't played and doesn't exist.
        if(PIN_PORT & (1 << OUT_A))
        {
            initSerial(OUT_A);
            mp3_stop();
            continue;
        } //Try again
        else
        {
            initSerial(OUT_A);
            mp3_stop();
            //initSystemClock();
            _delay_ms(100);
            num++;
        }
    }
    return num - 1;
}

void play_random(uint8_t volume)
{
    uint8_t track;
    char buff[] = "01234567890123";
    save_regs();
    backgound_playing = 0;
    initSerial(OUT_A); _delay_ms(10);
    mp3_set_volume(volume);
    stop_repeat();
    _delay_ms(50);
    if(file_count < 2) track = 1;
    else track = gen_random(file_count);
    // DEBUGGING
    sprintf(buff, "\nrand = %d\n",track);
    sendStrBySerial(buff);
    sendBySerial(0xEF);

    DDR_PORT |= (1 << OUT_A); // Set pin to output.
    mp3_play_in_mp3 (track);
    DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(100);

    // Revert to normal timer
    initSystemClock();
    // and restore counters
    restore_regs();
    // PCINT0 can be/is connected to BUSY pin
    DDR_PORT &= ~(1 << OUT_A); // Set it as input so we can monitor it's status.
}

void play_as_background(uint8_t track, uint8_t volume)
{
    save_regs();
    PCMSK1 &= ~(1 << PCINT0); // Disable PCINT0
    initSerial(OUT_A);
    _delay_ms(10);
    mp3_set_volume(volume);
    mp3_play_in_mp3 (track);
    start_repeat(); // Repeat currently playing track
    _delay_ms(100);
    // Revert to normal timer
    initSystemClock();
    // and restore counter
    restore_regs();
    DDR_PORT &= ~(1 << OUT_A); // So set it as input
}

/**
Play a specified frack in the mp3 folder
@param uint8_t track_num
@param uint8_t volume
*/
void play_track(uint8_t track_num, uint8_t volume)
{
    // Save the counter
    save_regs();
    //uint32_t timer;
    initSerial(OUT_A); _delay_ms(10);
    mp3_set_volume(volume);
    mp3_play_in_mp3 (track_num);
    DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(100);
    initSystemClock(); // So we can add a timeout.
    restore_regs();
    //timer = millis();
}

void set_volume(uint8_t volume)
{
    save_regs();
    initSerial(OUT_A); _delay_ms(10);
    mp3_set_volume(volume);
    DDR_PORT &= ~(1 << OUT_A); // Set pin to input.
    _delay_ms(100);
    initSystemClock(); // So we can add a timeout.
    restore_regs();
}
