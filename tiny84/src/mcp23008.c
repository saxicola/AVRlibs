




#include "i2cmaster.h"
#include "mcp23008.h"



void mcp23008_set_iodir(uint8_t pins)
{
	unsigned char retval = 0;
    i2c_start((MCP23008_ADDR << 1) | MCP23008_IODIR);
    i2c_write(pins);
    i2c_stop();
    //return retval;
}


void mcp23008_write(uint8_t pins)
{
	unsigned char retval = 0;
    i2c_start((MCP23008_ADDR << 1) | MCP23008_GPIO);
    i2c_write(pins);
    i2c_stop();
    //return retval;
}
