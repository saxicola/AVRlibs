/*
 * The information needed to write this file was from
 * https://github.com/jorgegarciadev/Arduinonexus/blob/master/Arduinonexus.cpp
 * Thank you Jorge García for posting it on GitHub
 */



#include <avr/io.h>
#include <util/delay.h>
#include <util/crc16.h>
#include "nexus_tx.h"

#define TX_PORT     PORTA
#define TX_PIN      PA1
#define TX_PWR      PB2
#define TX_DELAY    _delay_ms(2)

#define BIT_START   _delay_us(500)
#define ONE_DELAY   _delay_us(1800)
#define ZERO_DELAY  _delay_us(800)




uint64_t
_parse(uint8_t id, int battery, int channel, int temp, uint16_t hum)

{
    uint64_t        packet = 0;
    int             temperature = temp * 10;
    int             water = hum;
    packet = (packet << 8)  | id;
    packet = (packet << 1)  | battery;
    packet = (packet << 1) ;    // 1 bit 0 padding
    packet = (packet << 2)  | channel;
    packet = (packet << 12) | temperature;
    packet = (packet << 4)  | 0xA0;    // 4 bits 1 padding
    packet = (packet << 16) | water;
    return packet;
}


void
nexus_send(uint8_t id, int temperature, uint16_t water, int battery)

{
    uint64_t packet = _parse(id, battery, 0, temperature, water);
    DDRA |= _BV(TX_PORT); // Set to output
    DDRB |= _BV(TX_PWR);    // Set to output
    PORTB |= _BV(TX_PWR); // Power up the transmitter
    _delay_ms(500); // Wait a bit.
    packet &= ~_BV(44);     // bitClear(packet, );

    for (int i = 0; i < 13; i++)
    {
        int             bit;
        TX_PORT &= ~_BV(TX_PIN);  // digitalWrite(pin, LOW);
        _delay_us(4000);
        for (int j = 43; j >= 0; j--)
        {
            bit = (packet >> j) & 1;
            if (bit == 1)
            {
                // One,
                TX_PORT |= _BV(TX_PIN);
                BIT_START;
                TX_PORT &= ~_BV(TX_PIN);
                ONE_DELAY;
            }
            else
            {
                // zero
                TX_PORT |= _BV(TX_PIN);
                BIT_START;
                TX_PORT &= ~_BV(TX_PIN);
                ZERO_DELAY;
            }
        }

        TX_PORT |= _BV(TX_PIN);
        BIT_START;
        TX_PORT &= ~_BV(TX_PIN);
        ZERO_DELAY;
        // traling 0
    }
    TX_PORT &= ~_BV(TX_PIN);
    PORTB &= ~_BV(TX_PWR); // Power off the transmitter
    DDRA &= ~_BV(TX_PORT); // Set to INPUT
    DDRB &= ~_BV(TX_PWR);    // Set to INPUT
}
